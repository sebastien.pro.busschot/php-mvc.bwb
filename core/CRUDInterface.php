<?php
    interface CRUDInterface {
        public function retrieve($id_cible);
        public function update($id_cible);
        public function create($property);
        public function delete($id_cible);
    }
?>