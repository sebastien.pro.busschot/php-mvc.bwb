<?php
    class DAOAuto extends DAO {

        protected $_pdo;
        protected $_table;

        public function retrieve($id_cible){

        }
        public function update($id_cible){

        }
        public function create($property){

        }
        public function delete($id_cible){

        }
        public function getAll(){
            $res = $this->_pdo->query("SELECT * FROM $this->_table");
            return $res;
        }
        public function getAllBy($tableau_arguments){
            // // créer la partie where => tab1=tab2 AND ...


            // $res = $this->_pdo->query("SELECT * FROM $this->_table WHERE brand='Renault'");
            // //var_dump($res);
            // return $res;
        }

        public function __construct($table, $json_file){
            $this->_table = $table;
            $json_database = file_get_contents("./config/$json_file");
            $json_object = json_decode($json_database);
            //var_dump($json_object->dbname);
            $dsn = "$json_object->driver:host=$json_object->host;dbname=$json_object->dbname;charset=utf8";
            $this->_pdo = new PDO( $dsn, $json_object->username, $json_object->password);
            
        }
    }
