<?php
    abstract class DAO implements CRUDInterface, RepositoryInterface {

        protected $_pdo;
        protected $_table;

        abstract public function retrieve($id_cible);
        abstract public function update($id_cible);
        abstract public function create($property);
        abstract public function delete($id_cible);

        public function getAll(){
            $res = $this->_pdo->query("SELECT * FROM $this->_table");
            return $res;
        }

        abstract public function getAllBy($tableau_arguments);

        public function __construct($table, $json_file){
            $this->_table = $table;
            $json_database = file_get_contents("./config/$json_file");
            $json_object = json_decode($json_database);
            //var_dump($json_object->dbname);
            $dsn = "$json_object->driver:host=$json_object->host;dbname=$json_object->dbname;charset=utf8";
            $this->_pdo = new PDO( $dsn, $json_object->username, $json_object->password);
            
        }
    }
?>